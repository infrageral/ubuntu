#!/bin/bash
###
## Select URI of Web Page 
## https://www.oracle.com/database/technologies/instant-client/downloads.html
###
OC_BASIC=https://download.oracle.com/otn_software/linux/instantclient/19600/instantclient-basic-linux.x64-19.6.0.0.0dbru.zip
OC_SDK=https://download.oracle.com/otn_software/linux/instantclient/19600/instantclient-sdk-linux.x64-19.6.0.0.0dbru.zip

OC_BASIC_PATH="/tmp/instantclient-basic.zip"
OC_SDK_PATH="/tmp/instantclient-sdk.zip"

ROOT_DIR="/opt/oracle"

[ $UID -ne 0 ] && echo "Executar como usuario ROOT" && exit 1;

##
## Este script só gera o OCI8, não gera o PDO_OCI (php mais basicão)
##

which php;
[ $? -ne 0 ] && echo "PHP não encontrado" && exit 1;

# checando se é Ubuntu
`grep "^NAME=" /etc/os-release | grep "Ubuntu" -i  > /dev/null`;
[ $? -ne 0 ] && echo "Sistema não é Ubuntu 18/20" && exit 1;

# major version
UBUNTU_VERSION=`grep "VERSION_ID" /etc/os-release | grep -Eo "([0-9]+)" | head -n 1`;
# major e minor version
PHP_VERSION=`php -v | tac | tail -n 1 | cut -d " " -f 2 | cut -c 1-3`;

if [ ! -f ${OC_BASIC_PATH} ]; then
	wget ${OC_BASIC} -O ${OC_BASIC_PATH}
else
	echo "Arquivo encontrado BASIC IOC: ${OC_BASIC_PATH}"
fi
if [ ! -f ${OC_SDK_PATH} ]; then
	wget ${OC_SDK} -O ${OC_SDK_PATH}
else
	echo "Arquivo encontrato SDK IOC: ${OC_SDK_PATH}"
fi

rm ${ROOT_DIR}/instantclient -rf

## update && upgrade
apt update && apt -y upgrade && apt -y autoremove

## aplicacoes
apt -y install unzip

unzip -o -d ${ROOT_DIR}/ ${OC_BASIC_PATH}
unzip -o -d ${ROOT_DIR}/ ${OC_SDK_PATH}

mv ${ROOT_DIR}/instantclient_* ${ROOT_DIR}/instantclient

chown -R root.www-data ${ROOT_DIR};

# requerdo pelo OCI8.SO => libaio1
apt -y install php${PHP_VERSION}-dev php-pear build-essential libaio1

echo ${ROOT_DIR}/instantclient > /etc/ld.so.conf.d/oracle-instantclient.conf
ldconfig

###
## compilando
###
echo "instantclient,${ROOT_DIR}/instantclient" | pecl install oci8

[ ! -d /etc/php/${PHP_VERSION} ] && echo "PHP \"${PHP_VERSION}\" não existe: " && exit 1

if [ ! -f /etc/php/${PHP_VERSION}/mods-available/oci.ini ]; then
	echo "extension = oci8.so" > /etc/php/${PHP_VERSION}/mods-available/oci.ini
fi

## cli
[ ! -f /etc/php/${PHP_VERSION}/cli/conf.d/20-oci.ini ] && ln -s /etc/php/${PHP_VERSION}/mods-available/oci.ini /etc/php/${PHP_VERSION}/cli/conf.d/20-oci.ini
## fpm
[ ! -f /etc/php/${PHP_VERSION}/fpm/conf.d/20-oci.ini ] && ln -s /etc/php/${PHP_VERSION}/mods-available/oci.ini /etc/php/${PHP_VERSION}/fpm/conf.d/20-oci.ini


###
## environment
###
## no 18, exige o EXPORT, no 20 nao precisa mais
PREFIXO=`[ ${UBUNTU_VERSION} -lt 20 ] && echo "export " || echo ""`

grep "LD_LIBRARY_PATH=" /etc/environment > /dev/null
if [ $? -ne 0 ]; then
	echo "${PREFIXO}LD_LIBRARY_PATH=\"${ROOT_DIR}/instantclient\"" >> /etc/environment
fi
grep "ORACLE_HOME=" /etc/environment > /dev/null
if [ $? -ne 0 ]; then
	echo "${PREFIXO}ORACLE_HOME=\"${ROOT_DIR}/instantclient\"" >> /etc/environment
fi

systemctl restart "php${PHP_VERSION}-fpm"

# apaga arquivos desnecessários

apt -y autoremove "php${PHP_VERSION}-dev" php-pear build-essential
apt-get clean
rm ${OC_BASIC_PATH} ${OC_SDK_PATH}

# criado pelo PECL
rm /tmp/pear -rf

echo ""
echo ""
echo "##############################"
echo ""
echo "php -r \"echo function_exists('oci_connect')?'OCI8 instalado':'OCI8 não instalado';\""
echo "Resultado: " `php -r "echo function_exists('oci_connect')?'OCI8 instalado':'OCI8 não instalado';"`
echo ""
echo "##############################"
