##
# Para as etapas abaixo, já considero que o Linux esteja em Domínio via SSSD
#     https://bitbucket.org/infrageral/ubuntu/src/master/sssd.sh
##
[ $UID -ne 0 ] && echo "Script deve ser executado como ROOT" && exit 1

##
# Configuração do Grupo que é permitido fazer autenticação no NGINX (location)
##
cat << EOF > /etc/nginx/auth_pam_scsprestador
linux-nginx-scsprestador@unimedpalmas
EOF
chmod 644 /etc/nginx/auth_pam_scsprestador
##
# Confiugração do arquivo PAM usado pelo ngx_http_auth_pam
##
cat << EOF > /etc/pam.d/nginx_scsprestador
auth        required pam_listfile.so onerr=fail item=group sense=allow file=/etc/nginx/auth_pam_scsprestador
auth [success=done default=ignore] pam_succeed_if.so quiet_success uid > 2000
auth        required pam_sss.so
account     required pam_sss.so

##
# O de baixo ativa logins do SSS e do proprio LINUX
##
#auth        required pam_listfile.so onerr=fail item=group sense=allow file=/etc/nginx/auth_pam_scsprestador
#auth [success=done default=ignore] pam_succeed_if.so quiet_success uid > -1
#@include    common-auth
#@include    common-account
EOF

##
# NGINX 
##

# instalação do módulo adicional
apt install libnginx-mod-http-auth-pam

echo 'Configuração adicional para o NGINX'
echo ''
echo 'auth_pam              "Autenticacao Prestador - Unimed Palmas";'
echo '# /etc/pam.d/nginx_scsprestador'
echo 'auth_pam_service_name "nginx_scsprestador";'
echo ''

