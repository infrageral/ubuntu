#!/bin/bash
##
## Script responsavel por criar um serviço WebDav (WsgiDAV em python)
## 
## 	port: 9800
## 	/var/www
## 	/var/www/wsgidav
## 	/var/www/wsgidav/certs
##
## 	/etc/pam.d/wsgidav
## 	/etc/systemd/system/wsgidav.service
## 	/etc/rsyslog.d/wsgidav_service.conf
## 

[ $UID -ne 0 ] && echo "Script deve ser executado como ROOT" && exit 1

FORCE_REINSTALL="" # ou em branco
SSL_ON="" # ou em branco
WSGIDAV_PAM_COMMON="on" # ou em branco

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "FORCE_REINSTALL = ${FORCE_REINSTALL:-off}"
echo "SSL = ${SSL_ON:-off}"
echo "WSGIDAV_PAM_COMMON = ${WSGIDAV_PAM_COMMON:-off}"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
read -p "Caso a configuração esteja correta, pressione qualquer tecla para prosseguir"

WSGIDAV_PORT="9800"
WSGIDAV_ROOT="/var/www"

WSGIDAV_CONF_DIR="${WSGIDAV_ROOT}/wsgidav"
WSGIDAV_CONF="${WSGIDAV_CONF_DIR}/wsgidav.yaml"
WSGIDAV_LOG="${WSGIDAV_CONF_DIR}/log-wsgidav.log"

WSGIDAV_SERVICE_USER="www-data"
WSGIDAV_BIN=`which wsgidav`
WSGIDAV_SERVICE_NAME="wsgidav.service"
WSGIDAV_SERVICE="/etc/systemd/system/${WSGIDAV_SERVICE_NAME}"
WSGIDAV_RSYSLOG="/etc/rsyslog.d/wsgidav_service.conf"

WSGIDAV_PAM_GROUPS_ALLOW="www-data,linux-webdav@unimedpalmas"
WSGIDAV_PAM_SERVICE="wsgidav"
WSGIDAV_PAM_FILE="/etc/pam.d/${WSGIDAV_PAM_SERVICE}"
WSGIDAV_PAM_FILTER_FILE="${WSGIDAV_CONF_DIR}/pam_filter_groups_allowed"

SSL_CERT_DIR="${WSGIDAV_CONF_DIR}/certs";
SSL_CERT_CONF="${SSL_CERT_DIR}/selfsigned.conf";
SSL_CERT_KEY="${SSL_CERT_DIR}/selfsigned.key";
SSL_CERT="${SSL_CERT_DIR}/selfsigned.crt";

if [ ! -z ${SSL_ON} ]; then
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	echo "Confirme se os IPs estao corretos antes de prosseguir"
	echo ""
	hostname --ip-address
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	read -p "Caso os IPs estejam corretos, pressione qualquer tecla para prosseguir"
fi

## checando se binario esta instalado
if [ -z ${WSGIDAV_BIN} ]; then
    echo "Instalando dependencias: apt install -y python3-pip python3-lxml"
    apt update && apt install -y python3-pip python3-lxml
    
    echo "Instalando binarios: pip3 install wsgidav lxml python-pam # gevent or cheroot"
    pip3 install --no-cache wsgidav lxml python-pam gevent #cheroot
        
    ## tentando de novo
    WSGIDAV_BIN=`which wsgidav`
    
    ## se nao instalou... chao
    [ -z ${WSGIDAV_BIN} ] && echo "Falha ao instalar o 'wsgidav'" && exit 1;
	
	## aplicando PATCH no utils.py do WSGIDAV
	PATCH_UTILS=$(cat <<EOF.F
NDExYzQxMQo8ICAgICBlbHNlOg0KLS0tCj4gICAgIGVsaWYgaGFzYXR0cihzLCAnZW5jb2RlJyk6
DQo0MTJhNDEzLDQxNAo+ICAgICBlbHNlOg0KPiAgICAgICAgIHMgPSBjb21wYXQudG9fbmF0aXZl
KHMpLmVuY29kZShlbmNvZGluZ190bywgZXJyb3JzPWVycm9ycykuZGVjb2RlKGVuY29kaW5nX3Rv
KQ0K
EOF.F
)

	printf "%s" $PATCH_UTILS | base64 -d > ${WSGIDAV_CONF_DIR}/util.py.patch

	# aplicando correcao
	patch --silent --no-backup-if-mismatch --reject-file=- $(python3 -c 'from wsgidav import util; print(util.__file__)') ${WSGIDAV_CONF_DIR}/util.py.patch

fi

echo "Binario encontrado: ${WSGIDAV_BIN}"

## usuario existe?
id ${WSGIDAV_SERVICE_USER} || `echo "Usuario [${WSGIDAV_SERVICE_USER}] nao existe. Favor criá-lo" && exit 1`

echo "Verificando existencia da pasta: ${WSGIDAV_ROOT}"
[ ! -d ${WSGIDAV_ROOT} ] && echo "criando-a..." && mkdir -p ${WSGIDAV_ROOT}

echo "Verificando existencia da pasta: ${WSGIDAV_ROOT}/html"
[ ! -d ${WSGIDAV_ROOT}/html ] && echo "criando-a..." && mkdir -p ${WSGIDAV_ROOT}/html

chmod 775 ${WSGIDAV_ROOT}/html && chown -R ${WSGIDAV_SERVICE_USER}:${WSGIDAV_SERVICE_USER} ${WSGIDAV_ROOT}/html

echo "Verificando existencia da pasta: ${WSGIDAV_CONF_DIR}"
[ ! -d ${WSGIDAV_CONF_DIR} ] && echo "criando-a" && mkdir -p ${WSGIDAV_CONF_DIR}

## criando SERVICO
if [ ! -f ${WSGIDAV_SERVICE} ] || [ ! -z ${FORCE_REINSTALL} ]; then
    echo "Criando servico: ${WSGIDAV_SERVICE}"
    cat <<EOF > ${WSGIDAV_SERVICE}
[Unit]
Description=WsgiDAV WebDAV server
After=network.target

[Service]
User=${WSGIDAV_SERVICE_USER}
Group=${WSGIDAV_SERVICE_USER}
UMask=002
Type=simple
ExecStart=${WSGIDAV_BIN} -c ${WSGIDAV_CONF}
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=wsgidav_service

[Install]
WantedBy=multi-user.target
EOF

    echo "Criando SYSLOG: ${WSGIDAV_RSYSLOG}"
    cat <<EOF > ${WSGIDAV_RSYSLOG}
if \$programname == 'wsgidav_service' then ${WSGIDAV_LOG}
 & stop
EOF

	touch ${WSGIDAV_RSYSLOG}
	chown ${WSGIDAV_SERVICE_USER}:adm ${WSGIDAV_RSYSLOG}

    echo "Criando PAM filter: ${WSGIDAV_PAM_FILTER_FILE}"
    echo "Grupos permitidos: ${WSGIDAV_PAM_GROUPS_ALLOW}"
    cat <<EOF > ${WSGIDAV_PAM_FILTER_FILE}
$(echo ${WSGIDAV_PAM_GROUPS_ALLOW} | tr "," "\n")
EOF

    echo "Criando PAM auth: ${WSGIDAV_PAM_FILE}"
    
    if [ "${WSGIDAV_PAM_COMMON}" == "on" ]; then
    
    cat <<EOF > ${WSGIDAV_PAM_FILE}
auth        required pam_listfile.so onerr=fail item=group \\
                sense=allow file=${WSGIDAV_PAM_FILTER_FILE}
auth [success=done default=ignore] pam_succeed_if.so quiet_success uid > -1
#auth        required pam_sss.so
#account     required pam_sss.so
@include    common-auth
@include    common-account
EOF

    else
    cat <<EOF > ${WSGIDAV_PAM_FILE}
auth        required pam_listfile.so onerr=fail item=group \\
                sense=allow file=${WSGIDAV_PAM_FILTER_FILE}
auth [success=done default=ignore] pam_succeed_if.so quiet_success uid > 2000
auth        required pam_sss.so
account     required pam_sss.so
#@include    common-auth
#@include    common-account
EOF
    fi
    
    echo "Reiniciando systemctl"
    systemctl daemon-reload
    
    echo "Ativando servico: ${WSGIDAV_SERVICE}"
    systemctl enable ${WSGIDAV_SERVICE_NAME}
fi

## criando Certs, se SSL
if [ ! -z ${SSL_ON} ]; then
    echo "Verificando a existencia do certificado: ${SSL_CERT}"
    if [ ! -f ${SSL_CERT} ] || [ ! -f ${SSL_CERT_KEY} ] || [ ! -z ${FORCE_REINSTALL} ]; then
        echo "Criando certificado..."
        mkdir -p ${SSL_CERT_DIR}
        cat <<EOF > ${SSL_CERT_CONF}
[req]
default_bits = 4096
distinguished_name = req_distinguished_name
req_extensions = v3_req
prompt = no

[req_distinguished_name]
C = BR
ST = TO
L = Palmas
O = Unimed TI-N2
OU = TI-N2
CN = $(hostname --fqdn)
emailAddress = aranna@unimedpalmas.com.br

[v3_ca]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer:always
basicConstraints = CA:true

[v3_req]
# Extensions to add to a certificate request
basicConstraints = CA:FALSE
keyUsage = digitalSignature, keyEncipherment
# alternative names by ENV
subjectAltName = \$ENV::IPS_ADDRESS
EOF

        ## capture IPS
        ## filter list
        ## enumerate list
        ## make string
        ## join array to string
        export IPS_ADDRESS=$(python3 -c 'import sys;print(",".join(map(lambda y:"DNS:{0}".format(y), filter(lambda x:x,sys.argv[1:]))))' `hostname --ip-address` `hostname --short`)    
        
        echo "IPs/DNS que serao utilizados: ${IPS_ADDRESS}"
        
        openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -keyout ${SSL_CERT_KEY} \
        -out ${SSL_CERT} \
        -config ${SSL_CERT_CONF} \
        -extensions v3_req  && \
        unset IPS_ADDRESS
		
		chmod o+r ${SSL_CERT_KEY}
    fi
fi

if [ ! -f ${WSGIDAV_CONF} ] || [ ! -z ${FORCE_REINSTALL} ]; then
	
    ## checa se pasta existe
    [ ! -d "${WSGIDAV_ROOT}/nginx-logs" ] && mkdir "${WSGIDAV_ROOT}/nginx-logs" && chown www-data.www-data "${WSGIDAV_ROOT}/nginx-logs";
    
	echo "Criando arquivo de configuracao: ${WSGIDAV_CONF}"
	cat <<EOF > ${WSGIDAV_CONF}
#server: "cheroot"
server: "gevent"

# Server specific arguments, for example
# cheroot: https://cheroot.cherrypy.org/en/latest/pkg/cheroot.wsgi.html#cheroot.wsgi.Server
server_args:
     numthreads: 200

host: 0.0.0.0
port: ${WSGIDAV_PORT}
block_size: 8192
add_header_MS_Author_Via: true

hotfixes:
    winxp_accept_root_share_login: false
    win_accept_anonymous_options: false
    unquote_path_info: false
    #: (See issue #73) Set null to activate on Python 3 only
    re_encode_path_info: null
    emulate_win32_lastmod: true

$([ ! -z ${SSL_ON} ] && echo "ssl_certificate: ${SSL_CERT}" || echo "# ssl_certificate: null")
$([ ! -z ${SSL_ON} ] && echo "ssl_private_key: ${SSL_CERT_KEY}" || echo "# ssl_private_key: null")
# ssl_certificate_chain: null

#: Cheroot server supports 'builtin' and 'pyopenssl' (default: 'builtin')
# ssl_adapter: "pyopenssl"

provider_mapping:
    "/": "${WSGIDAV_ROOT}"
# --------------
#    "/": "${WSGIDAV_ROOT}/html"
#    "/nginx-logs": "${WSGIDAV_ROOT}/nginx-logs"
#    "/public":
#       root: "/public_folder"
#       readonly: true

http_authenticator:
    accept_basic: true
    accept_digest: false
    default_to_digest: false
    domain_controller: wsgidav.dc.pam_dc.PAMDomainController

# Additional options for PAMDomainController only:
pam_dc:
    service: "${WSGIDAV_PAM_SERVICE}"
    encoding: "utf-8"
    resetcreds: false

#: 0 CRITICAL   quiet
#: 1 ERROR      no output (excepting application exceptions)
#: 2 WARN       warnings and errors only
#: 3 INFO       (default)show single line request summaries (for HTTP logging)
#: 4 DEBUG      show additional events
#: 5 DEBUG      show full request/response header info (HTTP Logging) request body and GET response bodies not shown
# log file: ${WSGIDAV_LOG}
verbose: 0

logger_format: "%(asctime)s.%(msecs)03d - %(levelname)-8s: %(message)s"
logger_date_format: "%H:%M:%S"

#: Let ErrorPrinter middleware catch all exceptions to return as 500 Internal Error
error_printer:
    catch_all: false

dir_browser:
    enable: true
    #: List of fnmatch patterns that will be hidden in the directory listing
    ignore:
        - ".DS_Store"  # macOS folder meta data
        - "Thumbs.db"  # Windows image previews
        - "._*"  # macOS hidden data files
    #: Display WsgiDAV icon in header
    icon: true
    #: Raw HTML code, appended as footer (true: use a default trailer)
    response_trailer: true
    #: Display the name and realm of the authenticated user (or 'anomymous')
    show_user: true
    show_logout: true
    #: Send <dm:mount> response if request URL contains '?davmount'
    #: Also add a respective link at the top of the listing
    #: (See https://tools.ietf.org/html/rfc4709)
    davmount: false
    ms_mount: false
    ms_sharepoint_support: true

property_manager: true
mutable_live_props:
    - "{DAV:}getlastmodified"

lock_manager: true
EOF

else
	echo "Arquivo de configuracao existente: ${WSGIDAV_CONF}"
fi

echo "Reiniciando servico"
systemctl restart ${WSGIDAV_SERVICE_NAME} && systemctl status ${WSGIDAV_SERVICE_NAME}
echo ""
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "CONFIG ${WSGIDAV_CONF}"
echo "HOST:PORT 0.0.0.0:${WSGIDAV_PORT}"
echo "SSL ${SSL_ON:-off}"
echo ""
echo "systemctl [service] ${WSGIDAV_SERVICE_NAME}"
echo " log: ${WSGIDAV_LOG}"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""

exit 0
