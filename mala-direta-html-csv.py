# -*- coding: utf8 -*-
from __future__ import print_function,unicode_literals
import smtplib
import codecs, re
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# ----------------------------------------------------------------------------------------
#
# Leiaute em HTML pode ser produzido no site: https://beefree.io/editor/?template=empty
#
# Os e-mails devem estar em linhas, os e-mails serão extraídos através de REGEX.
# Com essa tática, evito problemas com formato de e-mail inválido e, havendo outros
# cadastrados na mesma linha por (registro SQL como texto aberto), consigo capturá-los também
# 
# ----------------------------------------------------------------------------------------

FILE_HTML = 'aviso-unimed.html'
FILE_EMAILS = 'emails.csv'
FILE_ENVIADOS = 'enviados.txt'
FILE_NAOENVIADOS = 'nao-enviados.txt'

SMTP_HOST = 'smtplw.com.br'
SMTP_PORT = 587
SMTP_LOGIN = 'unimedpalmas2'
SMTP_PASS = '****'                  # Verificar em https://smtplw.com.br
SMTP_SENDER = 'naoresponda@unimedpalmas.com.br'

print("Carregando HTML")
with codecs.open(FILE_HTML, 'r', 'utf-8') as html:
	html_email = MIMEText(html.read(), 'html', 'utf-8')
	
if html_email is None:
	print("Erro ao gerar a mensagem a partir do HTML")
	exit(1)
	
print("Gerando corpo do e-mail")

print("Conectando ao servidor SMTP")
s = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
s.set_debuglevel(0)
s.login(SMTP_LOGIN, SMTP_PASS)
print( "Logado!")

# para cada LINHA do arquivo FILE_EMAILS
# extra o email (tem linhas com mais de 1 email separado por ';')
re_email = r'([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)'

print("Disparando e-mails")
with codecs.open(FILE_NAOENVIADOS, 'a+', 'utf-8') as naoenviados, codecs.open(FILE_ENVIADOS, 'a+', 'utf-8') as enviados:
    naoenviados.write('------------------\n')
    enviados.write('------------------\n')
	with codecs.open(FILE_EMAILS, 'r', 'utf-8') as csv:
		for idx,linha in enumerate(csv, start=1):
			# return list
			recipients = re.findall(re_email, linha.lower())
			if not recipients:
				naoenviados.write(linha)
                # volta para o FOR de novo
				continue
                
			print(idx, recipients)
			try:
				msg = MIMEMultipart('alternative')
				msg.attach(html_email)
				msg['Subject'] = 'Comunicado Importante - Unimed Palmas'
				msg['From'] = SMTP_SENDER
				msg['To'] = ', '.join(recipients)
				s.sendmail(SMTP_SENDER, recipients, msg.as_string())
				enviados.write(msg['To']+'\n')
				print(u'\r enviado')
			except Exception as e:
				print(u'\r NAO enviado')
				print(e)
				naoenviados.write("Erro no smtplib: {}".format(linha))
				s.quit()
                # reconectando por algum BUG anterior, no TRY
				s = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
				s.set_debuglevel(0)
				s.login(SMTP_LOGIN, SMTP_PASS)
			
print("Finalizando...")
s.quit()
print("Finalizado!")