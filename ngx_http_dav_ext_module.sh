#!/bin/bash

#
## Compilar o modulo com LOCK e UNLOCK option no WebDav
##
## Será necessário instalar dependencias para conseguir compilar
#
## Documentação oficial sobre compilar um módulo dinâmico
## https://gorails.com/blog/how-to-compile-dynamic-nginx-modules
#

# pega a atual versão do nginx, caso exista
echo "Verificando versao do NGINX"
nginx -v &> /tmp/nginx-version
NGINX_VERSION=`grep -Eo '([0-9]+\.[0-9]+\.[0-9]+)' /tmp/nginx-version`
rm /tmp/nginx-version

# se vazio... encerra
if [ -z ${NGINX_VERSION} ]; then
    echo "NGINX Version not found! NGINX is installed?"
    exit 1
fi

NGINX_BUILD_DIR="${HOME}/nginx-build"
read -p "1) Informe o diretorio para gerar o BUILDer [${NGINX_BUILD_DIR}]: " input_
NGINX_BUILD_DIR=${input_:-${NGINX_BUILD_DIR}}

mkdir -p ${NGINX_BUILD_DIR}

NGINX_SOURCE_TAR="${NGINX_BUILD_DIR}/nginx-${NGINX_VERSION}.tar.gz"
NGINX_SOURCE_DIR="${NGINX_BUILD_DIR}/nginx-${NGINX_VERSION}/"

NGINX_MODULE_DIR="${NGINX_BUILD_DIR}/ngx-http-dav-ext-module"

# se arquivo nao existe... baixa-o
if [ ! -f ${NGINX_SOURCE_TAR} ]; then

    # baixar o arquivo FONTE do NGINX na mesma vers  o do que est   instalado no sistema
    echo "Baixando arquivo fonte da versao ${NGINX_VERSION} atual: https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz"
    echo "Sanvando em: ${NGINX_SOURCE_TAR}"
    wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz -O ${NGINX_SOURCE_TAR}
fi

echo "Descompactando ${NGINX_SOURCE_TAR}"
tar -zxf ${NGINX_SOURCE_TAR} -C ${NGINX_BUILD_DIR}

# se nao gerou a pasta esperada... exit
[ ! -d ${NGINX_SOURCE_DIR} ] && echo "Arquivo nao descompactado em ${NGINX_SOURCE_DIR}" && exit 1

# o arquivo fonte do modulo 'ngx_http_dav_ext_module' existe?
if [ ! -d ${NGINX_MODULE_DIR} ]; then
    echo "Criando diretorio para o modulo 'ngx_http_dav_ext_module': ${NGINX_MODULE_DIR}" ;
    mkdir -p ${NGINX_MODULE_DIR}
    
    echo "Criando arquivo de configuracao do modulo 'ngx_http_dav_ext_module': ${NGINX_MODULE_DIR}/config"
    cat <<EOF > ${NGINX_MODULE_DIR}/config
ngx_addon_name=ngx_http_dav_ext_module

if test -n "\$ngx_module_link"; then
    ngx_module_type=HTTP
    ngx_module_name=ngx_http_dav_ext_module
    ngx_module_srcs="\$ngx_addon_dir/ngx_http_dav_ext_module.c"

    . auto/module
else
    HTTP_MODULES="\$HTTP_MODULES ngx_http_dav_ext_module"
    NGX_ADDON_SRCS="\$NGX_ADDON_SRCS \$ngx_addon_dir/ngx_http_dav_ext_module.c"
fi
EOF

    echo "Criando arquivo fonte do modulo 'ngx_http_dav_ext_module': ${NGINX_MODULE_DIR}/ngx_http_dav_ext_module.c"
    wget https://raw.githubusercontent.com/arut/nginx-dav-ext-module/master/ngx_http_dav_ext_module.c -O ${NGINX_MODULE_DIR}/ngx_http_dav_ext_module.c
    
    ls -l --color ${NGINX_MODULE_DIR}
fi

echo "Instalando dependencias: apt install -y nginx-extras"
apt install -y nginx-extras

echo "Instalando dependencia e utilitario 'xml2-config': apt install -y libxml2-dev libxslt1-dev"
apt install -y libxml2-dev libxslt1-dev

# buscando FLAGs do nginx compilado
# com excecao as --add-dynamic-module
echo "Buscando as FLAGs do NGINX atual 'nginx -V'"
[ ! -f /tmp/nginx-flags ] && `nginx -V &> /tmp/nginx-flags `

NGINX_FLAGS=$(NGINX_FLAGS=`sed -n 's/^configure arguments\: \(.*\)$/\1/p' /tmp/nginx-flags` \
python3 - <<EOF
import os, re
ngx_flags = os.environ.get('NGINX_FLAGS', '')
flags=re.findall(r"(--[\w\-]+[^\ ]+|--[\w\-]+=\'[\w\ \-\.\=\/]+\'|--[\w\-]+=[\w\-\.\/]+[^\ ]+)", ngx_flags)
flags=filter(lambda a: not u"{}".format(a).startswith('--add-dynamic-module'), flags)
print(u' '.join(flags))
EOF
)

# adiciona o modulo customizado
NGINX_FLAGS="${NGINX_FLAGS} --add-dynamic-module=\"${NGINX_MODULE_DIR}\" "
# adiciona flags do xml2-config
NGINX_FLAGS="${NGINX_FLAGS} --with-cc-opt=\"$(xml2-config --cflags)\" --with-ld-opt=\"$(xml2-config --libs)\" "

## Documentação oficial sobre compilar um módulo dinâmico
## https://gorails.com/blog/how-to-compile-dynamic-nginx-modules

echo "Instalando dependencias para o nginx-dev: apt install -y libperl-dev libgeoip-dev libgd-dev"
apt install -y libperl-dev libgeoip-dev libgd-dev

echo "Instalando dependencias para o nginx-dev: apt install -y gcc make libssl-dev"
apt install -y gcc make libssl-dev

## compilando
echo ""
echo ""
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "Execute os comandos abaixo para compliar (1), construir o modulo (2) e substituir o original (3)"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""

## construindo o modulo
echo "(a)"
echo "cd ${NGINX_SOURCE_DIR}"
echo ""
echo "(1)"
echo "./configure ${NGINX_FLAGS}"
echo ""
echo "(2)"
echo "make modules"
echo ""
echo "(3)"
echo "cp ./objs/ngx_http_dav_ext_module.so /usr/share/nginx/modules/ngx_http_dav_ext_module.so"
echo ""
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo ""
echo "----------------------------------"
echo "| exporte o arquivo para outro local"
echo "| ${NGINX_SOURCE_DIR}/objs/ngx_http_dav_ext_module.so"
echo "| pois este servidor deverá ser APENAS para compilar o arquivo acima"
echo "----------------------------------"
echo ""

exit 0
