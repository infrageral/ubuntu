import smtplib
from email.mime.text import MIMEText

PHOST = 'email-ssl.com.br'
PPORT = 465
PSSL = True
PTLS = False
PDEBUG = 1 # 0
PFROM = 'asdf@.com.br'
PUSER = PFROM
PPASS = ''

PTO = [
'arannasousa@gmail.com',
'aranna@unimedpalmas.com.br'
]

if PSSL is True:
 s = smtplib.SMTP_SSL(host=PHOST, port=PPORT)
 s.set_debuglevel(PDEBUG)
 s.ehlo()

else:
 s = smtplib.SMTP(host=PHOST, port=PPORT)
 s.set_debuglevel(PDEBUG)

if PTLS is True:
 s.starttls()

if PUSER:
 s.login(user=PUSER, password=PPASS)

msgm = MIMEText('Corpo do email')
msgm['Subject'] = 'Teste de e-mail'
msgm['From'] = PFROM
msgm['To'] = ', '.join(PTO)

s.sendmail(PFROM, PTO, msgm.as_string())
s.close()