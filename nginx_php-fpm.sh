#!/bin/bash

## usuario NGINX e PHP-fpm
WWWUSER="www-data"
WWWGROUP="www-data"
ROOT_DIR="/var/www/html"
ROOT_DIR_LOG="/var/www/nginx-logs"

## CodeIgniter enviroment:
##   development -> permite o uso de PASTAS com projetos diversos
##   testing
##   production
CODE_IGNITER_MODE='production'

## 5.6, 7.2, 7.4, em branco fica automático
PHP_VERSION=""

[[ $UID -ne 0 ]] && echo "Executar como usuario ROOT" && exit 1;

echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "CODE_IGNITER_MODE = ${CODE_IGNITER_MODE}"
echo "PHP_VERSION = ${PHP_VERSION:-auto}"
echo "--"
echo "WWWUSER = ${WWWUSER}"
echo "WWWGROUP = ${WWWGROUP}"
echo "ROOT_DIR = ${ROOT_DIR}"
echo "ROOT_DIR_LOG = ${ROOT_DIR_LOG}"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
read -p "Caso a configuração esteja correta, pressione qualquer tecla para prosseguir"

## update && upgrade
apt update && apt -y upgrade

## aplicacoes
apt install -y htop nano
apt install -y nginx
apt install -y php${PHP_VERSION}-fpm php${PHP_VERSION}-common php${PHP_VERSION}-mbstring php${PHP_VERSION}-gd php${PHP_VERSION}-xml php${PHP_VERSION}-intl php${PHP_VERSION}-mysql php${PHP_VERSION}-cli php${PHP_VERSION}-zip php${PHP_VERSION}-curl

apt -y autoremove

apt-get clean

## spool
rm -rf /var/spool/postfix/*

# major e minor version
PHP_VERSION=`php -v | tac | tail -n 1 | cut -d " " -f 2 | cut -c 1-3`;

###
## configuração Logs
###

## journal
# /etc/systemd/journald.conf
#	SystemMaxUse=1G
#	SystemMaxFileSize=10M
sed -in 's/^#\(SystemMaxUse\)=.*/\1=1G/g' /etc/systemd/journald.conf
sed -in 's/^#\(SystemMaxFileSize\)=.*/\1=10M/g' /etc/systemd/journald.conf
systemctl restart systemd-journald

# === POR PROBLEMAS DE SEGURANÇA, ESSE PACOTE NÃO ESTÁ VINDO NO UBUNTU 20 ===
# apt install -y rssh         <= pacote possui tantos bugs que foi retirado do Ubuntu 20.04
##
## Configuração RSSH
##
## Permitir SCP, RSYNC MAS não permite login
###
#WWWUSER_HOME=$(getent passwd "${WWWUSER}" | cut -d: -f6)
#WWWUSER_SHELL=$(getent passwd "${WWWUSER}" | cut -d: -f7)

#RSSH=$(which rssh)

## trocando SHELL
#if [ "${WWWUSER_SHELL}" != "${RSSH}" ]; then
#	echo "Trocando SHELL do usuário \"${WWWUSER}\" para: ${RSSH}"
#	usermod --shell ${RSSH} ${WWWUSER}
#fi

#
## necessário para o GIT clone/fetch/pull funcionar corretamente
## no passwd, o usuário está com /usr/sbin/nologin
#
## create KEY
if [ ! -f ${WWWUSER_HOME}/.ssh/id_rsa ]; then
	echo "Criando diretorio: ${WWWUSER_HOME}/.ssh/"
	mkdir -p ${WWWUSER_HOME}/.ssh/
	chown ${WWWUSER}.${WWWGROUP} ${WWWUSER_HOME}/.ssh/
    sudo -u ${WWWUSER} ssh-keygen -t rsa -N '' -f ${WWWUSER_HOME}/.ssh/id_rsa
fi

#if [ ! -f ${WWWUSER_HOME}/.ssh/authorized_keys ]; then
#	sudo -u ${WWWUSER} touch ${WWWUSER_HOME}/.ssh/authorized_keys
#    chmod 600 ${WWWUSER_HOME}/.ssh/authorized_keys
#fi

###
## Configuração PHP-fpm
###


###
## Configuração NGINX
###
NGINX_CONF="/etc/nginx/nginx.conf"

echo "Adequando o NGINX: ${NGINX_CONF}"

# server_tokens = off;
sed -in 's/^\([[:blank:]]\+\)# server_tokens off\;/\1server_tokens off\;/g' ${NGINX_CONF}
echo " server_tokens off;"

# access_log off;
sed -in 's/^\([[:blank:]]\+\)\(access_log \/var\/log\/nginx\/access.log\;\)/\1\# \2\n\1access_log off\;/g' ${NGINX_CONF}
echo " access_log off;"

[ ! -d ${ROOT_DIR_LOG} ] && \
echo "Criando diretorio de logs: ${ROOT_DIR_LOG}" && \
mkdir -p ${ROOT_DIR_LOG} && \
chown ${WWWUSER}.${WWWGROUP} ${ROOT_DIR_LOG};

chown ${WWWUSER}.${WWWGROUP} ${ROOT_DIR};

cat << EOF > /etc/nginx/sites-available/default
server {
        listen 80 default_server;

        server_name _;

        root ${ROOT_DIR};

        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm;

        location / {
                try_files \$uri \$uri/ /index.php?\$args;
        }

        location ~ \.php\$ {
                include snippets/fastcgi-php.conf;

                # With php-fpm (or other unix sockets):
                fastcgi_pass unix:/var/run/php/php${PHP_VERSION}-fpm.sock;
                # With php-cgi (or other tcp sockets):
                #fastcgi_pass 127.0.0.1:9000;
				
                fastcgi_send_timeout 300s;
                fastcgi_read_timeout 300s;
				
                # codeigniter framework
                fastcgi_param CI_ENV ${CODE_IGNITER_MODE:-testing};
        }

        # deny access to hidden files and folders
        # concurs with nginx's one
        #
        location ~ /\..* {
                deny all;
                access_log off;
                log_not_found off;
                return 404;
        }

        error_log ${ROOT_DIR_LOG}/error.log;
        access_log off; # ${ROOT_DIR_LOG}/access.log;
}
EOF

cat << EOF > /etc/nginx/sites-available/development
server {
        listen 80 default_server;

        server_name _;

        root ${ROOT_DIR};

        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm;
				
		# somente para desenvolvimento
        autoindex on;

        location / {
                #try_files \$uri \$uri/ /index.php?\$request_uri =404;
                try_files \$uri \$uri/ @ci_php_kickstart;
        }
		
        location @ci_php_kickstart {
                # procura por um prefixo /<este ponto>/resto-da-url-od-projeto
                rewrite ^(/[^/]+)(.+[^\.php])\$ \$1/index.php?\$2 last;
                # modelo comum achado na internet
                rewrite ^(.*) /index.php?\$1 last;
                return 404;
        }

        location ~ \.php\$ {
                include snippets/fastcgi-php.conf;

                # With php-fpm (or other unix sockets):
                fastcgi_pass unix:/var/run/php/php${PHP_VERSION}-fpm.sock;
                # With php-cgi (or other tcp sockets):
                #fastcgi_pass 127.0.0.1:9000;
				
                fastcgi_send_timeout 300s;
                fastcgi_read_timeout 300s;
				
                # codeigniter framework
                fastcgi_param CI_ENV development;
        }

        # deny access to hidden files and folders
        # concurs with nginx's one
        #
        location ~ /\..* {
                deny all;
                access_log off;
                log_not_found off;
                return 404;
        }

        error_log ${ROOT_DIR_LOG}/error.log;
        access_log ${ROOT_DIR_LOG}/access.log;
}
EOF


##
## logrotate NGINX custom
##
if [ ! -f /etc/logrotate.d/nginx-projeto ]; then
	cat << EOF >  /etc/logrotate.d/nginx-projeto
${ROOT_DIR_LOG}/*.log {
        daily
        missingok
        rotate 7
        compress
        delaycompress
        notifempty
        create 0640 www-data adm
        sharedscripts
        postrotate
                invoke-rc.d nginx rotate >/dev/null 2>&1
        endscript
}
EOF
fi

## configurando: CODE_IGNITER_MODE
if [ "${CODE_IGNITER_MODE}" == "development" ]; then
	rm /etc/nginx/sites-enabled/default
	ln -s  /etc/nginx/sites-available/development /etc/nginx/sites-enabled/default
else
	rm /etc/nginx/sites-enabled/default
	ln -s  /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
fi

echo "Reiniciando NGINX"
systemctl restart nginx

###
## SSSD - Domínio
## 
## [code] https://bitbucket.org/infrageral/ubuntu/src/master/wsgidav.sh
###
echo ""
echo ""
echo "################################################"
echo ""
echo "Para configurar o SSSD no sistema, baixar"
echo "https://bitbucket.org/infrageral/ubuntu/src/master/wsgidav.sh"

###
## WSGIDav
## 
## [code] https://bitbucket.org/infrageral/ubuntu/src/master/wsgidav.sh
###
echo ""
echo "################################################"
echo ""
echo "Para configurar o WSGIDav no sistema, baixar"
echo "https://bitbucket.org/infrageral/ubuntu/src/master/wsgida"
echo ""
echo "################################################"