#!/bin/bash

## -----------------------------------------------
## open firewall ports
## -----------------------------------------------
#
## OUTPUT
#
# DNS                     53    UDP and TCP      
# LDAP                   389    UDP and TCP      
# Kerberos                88    UDP and TCP      
# Kerberos               464    UDP and TCP     Used by kadmin for setting and changing a password
# LDAP Global Catalog   3268    TCP             If the id_provider = ad option is being used
# NTP                    123    UDP             Optional 
#
## -----------------------------------------------

echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
echo 'Obrigatorio verificar se o IP esta correto'
echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
cat /etc/hosts
echo '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
read -p "Pressione qualquer tecla para prosseguir"

echo "Atualizando pacotes"
apt update && apt upgrade

## configuracoes
SSS_LXC_UNPRIVILEGED="True"
REALM_DOMAIN="unimedpalmas.com.br"
SSS_FULL_LOGIN="False"      # incluir o @domininio para logar
SSS_FALLBACK_DIR="/home/users-ad"
SSS_CONFIG_DIR="/etc/sssd/sssd.conf"

read -p "1) Informe se esta em um CONTAINER UNPRIVILEGED (True or False)[${SSS_LXC_UNPRIVILEGED}]: " input_
SSS_LXC_UNPRIVILEGED=${input_:-${SSS_LXC_UNPRIVILEGED}}

read -p "2) Informe o DOMINIO local [${REALM_DOMAIN}]: " input_
REALM_DOMAIN=${input_:-${REALM_DOMAIN}}

read -p "3) Informe se @${REALM_DOMAIN} serah obrigatorio no login [${SSS_FULL_LOGIN}]: " input_
SSS_FULL_LOGIN=`[[ "True" == ${input_:-${SSS_FULL_LOGIN}} ]] && echo "True" || echo "False"`

SSS_GROUP_ALLOWED="linux-login"
SUDO_GROUP_ALLOWED="linux-sudo"
if [[ "True" == "${SSS_FULL_LOGIN}" ]]; then
    SSS_GROUP_ALLOWED="${SSS_GROUP_ALLOWED}@${REALM_DOMAIN}"
    SUDO_GROUP_ALLOWED="${SUDO_GROUP_ALLOWED}@${REALM_DOMAIN}"
fi

read -p "4) Informe qual GRUPO usado para PERMITIR 'login' [${SSS_GROUP_ALLOWED}]: " input_
SSS_GROUP_ALLOWED=${input_:-${SSS_GROUP_ALLOWED}}

read -p "5) Informe qual GRUPO usado para PERMITIR 'sudo' [${SUDO_GROUP_ALLOWED}]: " input_
SUDO_GROUP_ALLOWED=${input_:-${SUDO_GROUP_ALLOWED}}


## print com as configuracoes iniciais
echo ""
echo "Configuracoes informadas"
echo "SSS_LXC_UNPRIVILEGED = ${SSS_LXC_UNPRIVILEGED}"
echo "REALM_DOMAIN         = ${REALM_DOMAIN}"
echo "SSS_FULL_LOGIN       = ${SSS_FULL_LOGIN}"
echo "SSS_FALLBACK_DIR     = ${SSS_FALLBACK_DIR}"
echo "SSS_GROUP_ALLOWED    = ${SSS_GROUP_ALLOWED}"
echo "SUDO_GROUP_ALLOWED   = ${SUDO_GROUP_ALLOWED}"
echo ""

# confirma ??
read -p "Informacoes estao corretas? [True]: " input_
if [[ "True" != "${input_:-True}" ]]; then
   echo "Execute o script novamente! Bye"
   echo " "
   exit 1
fi

## dependences
echo "Instalando dependencias: policykit-1 sssd-tools sssd libnss-sss libpam-sss adcli realmd"
apt install -y packagekit policykit-1 sssd-tools sssd libnss-sss libpam-sss adcli realmd

## tool for edit ini/conf FILES (created in python)
echo "Instalando ferramentas: crudini"
apt install -y crudini

## join to domain
echo "Informe a senha do 'admin' do dominio"
realm join ${REALM_DOMAIN}

[ $? -gt 1 ] && echo "Fail! Nao foi possivel entrar no dominio: RealmD" && exit 1

## create default HOME dir
if [ ! -d ${SSS_FALLBACK_DIR} ]; then
    echo "Criando diretorio comum dos usuarios: ${SSS_FALLBACK_DIR}"
    mkdir -p ${SSS_FALLBACK_DIR}
    chmod 777 ${SSS_FALLBACK_DIR}
fi


## FILTER for restrict allowed USERS or GROUPS
## [domain/DOMAIN]
SSS_SECTION="domain/${REALM_DOMAIN}"

echo " access_provider = simple"
crudini --set "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "access_provider" "simple"

# adicionando o grupo permitido para fazer login
[ ! -z ${SSS_GROUP_ALLOWED} ] && echo "Grupo(s) permitido em fazer login: ${SSS_GROUP_ALLOWED}" && realm permit -g ${SSS_GROUP_ALLOWED}

##
## =====[edit /etc/sssd/sssd.conf]================
##

echo "Configurando o arquivo: ${SSS_CONFIG_DIR}"
echo ""

## login without domain part?
echo " use_fully_qualified_names = ${SSS_FULL_LOGIN}"
crudini --set --existing=param "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "use_fully_qualified_names" "${SSS_FULL_LOGIN}"

##
## change homedirectory
##
echo " fallback_homedir = ${SSS_FALLBACK_DIR}"
crudini --set "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "fallback_homedir" "${SSS_FALLBACK_DIR}"
#fallback_homedir = /tmp/users-ad    # non directory
#fallback_homedir = /home/%u        # user only
#fallback_homedir = /home/%u@%d     # user and domain

echo " ad_gpo_access_control = permissive"
crudini --set "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "ad_gpo_access_control" "permissive"

## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
## required LXC Unprivileged on Proxmox
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if [[ "True" == "${SSS_LXC_UNPRIVILEGED}" ]]; then
    echo " # Configs for LXC Unprivileged"

    echo " ldap_id_mapping = True"
    crudini --set --existing=param "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "ldap_id_mapping" "True"

    echo " ldap_idmap_range_min = 2000"
    crudini --set "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "ldap_idmap_range_min" "2000"
    echo " ldap_idmap_range_max = 65000"
    crudini --set "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "ldap_idmap_range_max" "65000"
    echo " ldap_idmap_range_size = 20"
    crudini --set "${SSS_CONFIG_DIR}" "${SSS_SECTION}" "ldap_idmap_range_size" "20"
fi
## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

## =====[end /etc/sssd/sssd.conf]================

## remove caches
echo ""
echo "Removendo caches: /var/lib/sss/db/ccache_* /var/lib/sss/db/cache_*"
rm /var/lib/sss/db/ccache_* /var/lib/sss/db/cache_*

## se for a nova versao do SSSD (2.2.x) presente no Ubuntu 20
## tenho que tirar qualquer servico configurado pois ele faz uso de Sockets
## caso contrário, ficará apresentando mensagens de erro no journal
if [ 1 -eq `sssd --version | grep -Eo '([0-9]+\.[0-9]+)' | xargs -I{} expr {} \>= 2.2` ]; then
    echo "Limpando '[sssd] services' para uso de sockets"
    crudini --set "${SSS_CONFIG_DIR}" "sssd" "services" ""
fi


## correção do bug https://bugs.launchpad.net/ubuntu/+source/sssd/+bug/1249777
## retirar o SSS da linha
##    sudoers: file sss
##    sudoers: file        < correto para evitar emails do tipo problem with defaults entries
sed -i '/sudoers\:/s/sss//' /etc/nsswitch.conf

## restart service
echo "Reiniciando servico: systemctl restart sssd"
systemctl restart sssd
systemctl status sssd

## configure SUDOERS
echo ""
echo "Criando o arquivo em sudoers.d: /etc/sudoers.d/linux-sudo-ad"
echo "Grupo permitido para SUDO: ${SUDO_GROUP_ALLOWED}"
cat <<EOF > /etc/sudoers.d/linux-sudo-ad
%${SUDO_GROUP_ALLOWED} ALL=(ALL) ALL
EOF

apt -y autoremove crudini

echo ""
echo "--------------------------------------"
echo "## OUTPUT firewall Rules"
echo ""
echo " DNS                     53    UDP and TCP"
echo " LDAP                   389    UDP and TCP"
echo " Kerberos                88    UDP and TCP"
echo " Kerberos               464    UDP and TCP     Used by kadmin for setting and changing a password"
echo " LDAP Global Catalog   3268    TCP             If the id_provider = ad option is being used"
echo " NTP                    123    UDP             Optional"
echo "--------------------------------------"
echo ""
echo "Configuracao concluido"
echo ""
echo "Reinicie a VM para aplicar as configuracoes 100%"
echo ""
exit 0
